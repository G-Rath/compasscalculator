import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Timer;
import java.util.TimerTask;

/**
 * main
 * Created by G-Rath on 8/05/2015.
 */
public class Main extends JPanel implements MouseListener
{
	public static Font monoFont = new Font( "Monospaced", Font.BOLD, 15 );
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenX = 640;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenY = 640;

	private Point firstPoint = new Point( screenX / 2, screenY / 2 );
	private Point secondPoint = new Point( screenX / 2, screenY / 2 );

	private String compassDirection = "none";
	private int compassPoints = 32;

	private double angle = 0;

	private String[] compassText = new String[32];

	/** Global timer that makes the program tick */
	@SuppressWarnings( "FieldCanBeLocal" )
	private Timer tickTimer;

	public Main()
	{
		setupCompassTextArray();
		setupFrame();
		setupTimer();
	}

	public static void main( String[] args )
	{
		new Main();
	}

	public static double round( double value, int places )
	{
		if( places < 0 ) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal( value );
		bd = bd.setScale( places, RoundingMode.HALF_UP );
		return bd.doubleValue();
	}

	public void setupCompassTextArray()
	{
		compassText[0] = "North";
		compassText[1] = "North by East";
		compassText[2] = "North-NorthEast";
		compassText[3] = "NorthEast by North";
		compassText[4] = "NorthEast";
		compassText[5] = "NorthEast by East";
		compassText[6] = "East-NorthEast";
		compassText[7] = "East by North";
		compassText[8] = "East";
		compassText[9] = "East by South";
		compassText[10] = "East-SouthEast";
		compassText[11] = "SouthEast by East";
		compassText[12] = "SouthEast";
		compassText[13] = "SouthEast by South";
		compassText[14] = "South-SouthEast";
		compassText[15] = "South by East";
		compassText[16] = "South";
		compassText[17] = "South by West";
		compassText[18] = "South-SouthWest";
		compassText[19] = "SouthWest by South";
		compassText[20] = "SouthWest";
		compassText[21] = "SouthWest by West";
		compassText[22] = "West-SouthWest";
		compassText[23] = "West by South";
		compassText[24] = "West";
		compassText[25] = "West by North";
		compassText[26] = "West-NorthWest";
		compassText[27] = "NorthWest by West";
		compassText[28] = "NorthWest";
		compassText[29] = "NorthWest by North";
		compassText[30] = "North-NorthWest";
		compassText[31] = "North by West";
	}

	public void setupFrame()
	{
		this.setPreferredSize( new Dimension( screenX, screenY ) );

		JFrame frame = new JFrame( "Compass Calculator" );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.add( this );
		frame.pack();
		frame.setLocationRelativeTo( null );
		frame.setVisible( true );

		this.addMouseListener( this );
	}

	@Override
	protected void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		doDrawing( g );
	}

	private void doDrawing( Graphics g )
	{
		Graphics2D g2d = (Graphics2D) g;

		int stringX = 1;

		angle = getAngle( firstPoint, secondPoint );
		compassDirection = calcCompassDirection();

		int compassNumber;
		double rotationAngle = 0;

		g2d.rotate( 5.62 );
//		g2d.drawLine( firstPoint.x, firstPoint.y, firstPoint.x, firstPoint.y - 50 );
		g2d.rotate( -5.62 );

//		while( !( angle > rotationAngle && angle < ( rotationAngle + 11.25 ) ) )
//		{
//			System.out.println( "\t" + angle + " > " + rotationAngle + " vs. " + angle + " < " + round( rotationAngle + 11.25, 2 ) );
//			rotationAngle += 11.25;
//			rotationAngle = round( rotationAngle, 2 );
//		}

		/*if( angle > 354.38 || angle < 5.62 )
			compassNumber = 0;
		else
		{
			while( !( angle > rotationAngle && angle < ( rotationAngle + 11.25 ) ) )
			{
				System.out.println( "\t" + angle + " > " + rotationAngle + " vs. " + angle + " < " + round( rotationAngle + 11.25, 2 ) );
				rotationAngle += 11.25;
				rotationAngle = round( rotationAngle, 2 );
			}
		}*/

		g2d.setColor( Color.RED );
		drawHighlightPoint( g2d, firstPoint );
		g2d.drawString( "P1: " + firstPoint.x + "x, " + firstPoint.y + "y", stringX, 15 );

		g2d.setColor( Color.BLUE );
		drawHighlightPoint( g2d, secondPoint );
		g2d.drawString( "P2: " + secondPoint.x + "x, " + secondPoint.y + "y", stringX, 30 );

		g2d.setColor( Color.RED );

		g2d.drawString( "Compass angle: " + angle, stringX, 45 );
		g2d.drawString( "Compass direction: " + compassDirection, stringX, 60 );

		Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
		g2d.dispose();                      //Dispose the copy of the Graphic we made
	}

	private void drawHighlightPoint( Graphics2D g2d, Point location )
	{
		g2d.drawOval( location.x - 10, location.y - 10, 20, 20 );
		g2d.drawOval( location.x - 1, location.y - 1, 2, 2 );
	}

	public void moveToNextPointCount()
	{
		switch( compassPoints )
		{
			case 8:
				compassPoints = 16;
				break;
			case 16:
				compassPoints = 32;
				break;
			case 32:
				compassPoints = 8;
				break;
			default:
				compassPoints = 8;
				break;
		}
	}

	public String calcCompassDirection()
	{
		int compassNumber = 0;

//		if( angle > 354.38 || angle < 5.62 )
//		{
//
//		}
//		else if( angle > 5.63 || angle < 16.87 )
//		{
//
//		}
//		else if( angle > 16.88 || angle < 28.12 )
//		{
//
//		}

		//System.out.println( angle );

		if( angle > 354.38 || angle < 5.62 )
			compassNumber = 0;
		else
		{
			double compassAngle = 5.63;

			System.out.println( "\r\nLoop" );
			System.out.println( "\t" + angle + " > " + compassAngle + " vs. " + angle + " < " + ( compassAngle + 11.25 ) );

			while( !( angle > compassAngle && angle < ( compassAngle + 11.25 ) ) )
			{
				System.out.println( "\t" + angle + " > " + compassAngle + " vs. " + angle + " < " + round( compassAngle + 11.25, 2 ) );
				compassAngle += 11.25;
				compassAngle = round( compassAngle, 2 );
				compassNumber++;

				if( compassNumber > 32 )
					return "failed";
			}
		}

		return compassText[compassNumber];
	}

	/** Set up the global tick timer that runs in this class to tick the whole program every 100ms */
	public void setupTimer()
	{
		tickTimer = new java.util.Timer( true );
		tickTimer.schedule( new TimerTask()
		{
			@Override
			public void run()
			{
				Main.this.repaint();
			}
		}, 50, 50 );
	}

	public float getAngle( Point source, Point target )
	{
		float angle = (float) Math.toDegrees( Math.atan2( target.y - source.y, target.x - source.x ) ) + 90;

		if( angle < 0 )
		{
			angle += 360;
		}

		return angle;
	}

	public void setFirstPointPosition( int x, int y )
	{
		firstPoint.setLocation( x, y );
	}

	public void setSecondPointPosition( int x, int y )
	{
		secondPoint.setLocation( x, y );
	}

	public void mouseClicked( MouseEvent e )
	{
		switch( e.getButton() )
		{
			case MouseEvent.BUTTON1:
				setFirstPointPosition( e.getX(), e.getY() );
				break;

			case MouseEvent.BUTTON2:
				break;

			case MouseEvent.BUTTON3:
				setSecondPointPosition( e.getX(), e.getY() );
				break;

			default:
				break;
		}

		System.out.println( e.getX() + "x, " + e.getY() + "y" );
	}

	public void mousePressed( MouseEvent e )
	{
		switch( e.getButton() )
		{
			case MouseEvent.BUTTON1:
				setFirstPointPosition( e.getX(), e.getY() );
				break;

			case MouseEvent.BUTTON2:
				break;

			case MouseEvent.BUTTON3:
				setSecondPointPosition( e.getX(), e.getY() );
				break;

			default:
				break;
		}
	}

	public void mouseReleased( MouseEvent e )
	{

	}

	public void mouseEntered( MouseEvent e )
	{

	}

	public void mouseExited( MouseEvent e )
	{

	}
}
